<?php
class Home_model extends CI_Model {
	function __construct(){
		parent::__construct();
	}
	function login($username,$password){
		$this->db->select('*');
		$this->db->from("user");
		$this->db->where('username =', $username);
		$this->db->where('password =', $password);
		$data = $this->db->count_all_results();
		return $data;
	}
	function cekantri($nim,$kode_surat){
		$this->db->select('*');
		$this->db->from("pengajuan");
		$this->db->where('nim =', $nim);
		$this->db->where('kode_surat =', $kode_surat);
		$this->db->where('status =', '1');
		$data = $this->db->count_all_results();
		return $data;
	}
	function getNomor($kode){
		$this->db->select('*');
		$this->db->from("kodeunit");
		$this->db->where('kodeunit =', $kode);
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getantri($nim){
		$this->db->select('*');
		$this->db->from("pengajuan");
		$this->db->where('nim =', $nim);
		$this->db->where('status !=', '0');
		$this->db->order_by("id", "desc");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getAjuan($id){
		$this->db->select('*');
		$this->db->from("pengajuan");
		$this->db->where('id =', $id);
		$this->db->where('status !=', '0');
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function getPrint($status = 0){
		$this->db->select('*');
		$this->db->from("pengajuan");
		if ($status != 0) {
			$this->db->where('status =', $status);
		}
		$this->db->where('status !=', '0');
		$this->db->order_by("tgl_submit", "desc");
		$query = $this->db->get();
		$data = $query->result_array();
		return $data;
	}
	function savePengajuan($data){
		$this->db->insert('pengajuan', $data);
	}

	function hapus($id,$data){
		$this->db->where('id', $id);
		$this->db->update('pengajuan', $data);
	}


}
?>
