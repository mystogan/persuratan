<?php
   $nim = $this->session->userdata('nim');
   $sex = $this->session->userdata('sex');
   $prodi = $this->session->userdata('prodi');
   $nama = $this->session->userdata('nama');
   $fakultas = $this->session->userdata('fakultas');

   function indo($date){
     $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
   	$tahun = substr($date, 0, 4);
   	$bulan = substr($date, 5, 2);
   	$tgl   = substr($date, 8, 2);
   	$jam  = substr($date, 11, 2);
   	$menit  = substr($date, 14, 2);
     // print_r ($BulanIndo[$bulan-1]);
     //  echo $jam."-".$menit;

     $data = $tgl." ".$BulanIndo[$bulan-1]." ".$tahun." ".$jam.":".$menit;
     return $data;
   }

    ?>
<div class="contentmenu" >
   <div class="container" >
      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="row">
            <div class="menu">
               <ul class="nav nav-tabs" style="padding-left:30%" role="tablist">
                  <li role="presentation"><a href="#surat" aria-controls="surat" role="tab" data-toggle="tab"><img title="Tambah Surat" style="width:3vh;margin-right:1vh;" src="<?php echo base_url();?>assets/images/1.png"/>Tambah Surat</a></li>
                  <li role="presentation" class="active"><a href="#status" aria-controls="status" role="tab" data-toggle="tab"><img title="Status" style="width:3vh;margin-right:1vh;" src="<?php echo base_url();?>assets/images/2.png"/>Status</a></li>
                  <li role="presentation"><a href="<?php echo base_url();?>logout"><img title="Status" style="width:3vh;margin-right:1vh;" src="<?php echo base_url();?>assets/images/logout.png"/>Keluar</a></li>
               </ul>
            </div>
         </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12" style="border-bottom:3px solid #eee; padding-bottom:3vh;padding-left:6vh;">
         <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6">
               <div class="row">
                  <div class="tempatNama">
                     <div class="nama1">NIM</div>
                     <div class="nama">: <?php echo $nim; ?></div>
                  </div>
                  <div class="tempatNama">
                     <div class="nama1">Nama </div>
                     <div class="nama">: <?php echo $nama; ?></div>
                  </div>
               </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
               <div class="row">
                  <div class="tempatNama">
                     <div class="nama1">Prodi </div>
                     <div class="nama">: <?php echo $prodi; ?></div>
                  </div>
                  <div class="tempatNama">
                     <div class="nama1">Fakultas </div>
                     <div class="nama">: <?php echo $fakultas; ?></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="content">
   <div class="container">
      <div class="col-xs-12 col-sm-12 col-md-12">
         <div class="row">
            <div class="tab-content">
               <div role="tabpanel" class="tab-pane" id="surat">
				<div class="alert alert-danger alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
				  <strong>Informasi!</strong> Jika Surat Belum Tersedia Pada Sistem, Silahkan Menghubungi Bagian Akademik .
				</div>
                  <ul class="thumbnails">
                     <li class="col-sm-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">1</div>
                           <div class="caption">
                              <h4>Surat Keterangan Aktif dan Bebas Beasiswa</h4>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <!-- <a  class="btn btn-success" href="<?php echo base_url() ?>prin/index/aktif_kuliah">» Buat Surat</a> -->
                              <a  class="btn btn-success" href="javascript:cek();">» Buat Surat</a>
                           </div>
                        </div>
                     </li>
                     <li class="col-sm-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">2</div>
                           <div class="caption">
                              <h4>Surat Rekomendasi</h4>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <a data-toggle="modal" data-target="#myModal2" class="btn btn-success" href="#">» Buat Surat</a>
                           </div>
                        </div>
                     </li>
                     <li class="col-sm-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">3</div>
                           <div class="caption">
                              <h4>Surat Permohonan Cuti Studi</h4>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <a class="btn btn-success" href="javascript:cekCuti();">» Buat Surat</a>
                           </div>
                        </div>
                     </li>
                     <li class="col-sm-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">4</div>
                           <div class="caption">
                              <h4>Surat Keterangan Aktif</h4>
                              <br>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <a class="btn btn-success" href="javascript:cekAktif();">» Buat Surat</a>
                           </div>
                        </div>
                     </li>
                     <li class="col-sm-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">5</div>
                           <div class="caption">
                              <h4>Surat Pengantar Magang</h4>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <a data-toggle="modal" data-target="#magang" class="btn btn-success" href="#">» Buat Surat</a>
                           </div>
                        </div>
                     </li>
                     <li class="col-sm-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">6</div>
                           <div class="caption">
                              <h4>Surat Pra Izin Penelitian</h4>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <a data-toggle="modal" data-target="#penelitian" class="btn btn-success" href="#">» Buat Surat</a>
                           </div>
                        </div>
                     </li>
                     <li class="col-sm-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">7</div>
                           <div class="caption">
                              <h4>Surat Permohonan Izin Penelitian</h4>
                              <p>Akademik <?php echo $fakultas; ?></p>
                              <a data-toggle="modal" data-target="#penelitian" class="btn btn-success" href="#">» Buat Surat</a>
                           </div>
                        </div>
                     </li>
                     <!-- <li class="col-sm-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">4</div>
                           <div class="caption">
                              <h4>Surat Keterangan Aktif Kuliah</h4>
                              <p>Akademik Fakultas Sains dan Teknologi</p>
                              <a data-toggle="modal" data-target="#myModal" class="btn btn-success" href="#">» Buat Surat</a>
                           </div>
                        </div>
                     </li>
                     <li class="col-sm-3">
                        <div class="fff">
                           <div class="thumbnail">
                              <a href="#"></a>
                           </div>
                           <div class="nomer">5</div>
                           <div class="caption">
                              <h4>Surat Keterangan Aktif Kuliah</h4>
                              <p>Akademik Fakultas Sains dan Teknologi</p>
                              <a data-toggle="modal" data-target="#myModal" class="btn btn-success" href="#">» Buat Surat</a>
                           </div>
                        </div>
                     </li> -->
                  </ul>
               </div>
               <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Surat Keterangan Aktif Mahasiswa</h4>
                        </div>
                        <div class="modal-body" id="modal_foto">
                           <div class="form-group">
                              <label for="nama" class="lb">Keterangan</label>
                              <textarea type="text" class="form-control" id="usr" value="Keperluan Surat" ></textarea>
                           </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Batas Tanggal</label>
                              <input type="date" class="form-control " id="tgl" name="tgl" />
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button class="btn btn-success">» Kirim</button>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="modal fade" id="magang" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Surat Pengantar Magang</h4>
                        </div>
                        <form class="" action="<?php echo base_url(); ?>prin/addMagang" method="post">
                        <div class="modal-body" >
                          <div class="form-group">
                            <label for="nama" class="lb">Nama Instansi</label>
                            <input type="text" class="form-control " id="instansi" name="instansi" />
                          </div>
                          <div class="form-group">
                            <label for="nama" class="lb">Alamat Instansi</label>
                            <input type="text" class="form-control " id="alamat" name="alamat" />
                          </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Tanggal Mulai</label>
                              <input type="date" class="form-control " id="mulai_tgl" name="mulai_tgl" />
                           </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Tanggal Akhir</label>
                              <input type="date" class="form-control " id="tgl_akhir" name="tgl_akhir" />
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button type="submit" class="btn btn-success">» Kirim</button>
                        </div>
                      </form>
                     </div>
                  </div>
               </div>
               <div class="modal fade" id="penelitian" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                     <div class="modal-content">
                       <form class="" action="<?php echo base_url(); ?>prin/addPenelitian" method="post">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                           <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Surat Pra Izin Penelitian</h4>
                        </div>
                        <div class="modal-body" id="modal_foto">
                          <div class="form-group">
                            <label for="nama" class="lb">Nama Instansi</label>
                            <input type="text" class="form-control " id="instansi" name="instansi" />
                          </div>
                          <div class="form-group">
                            <label for="nama" class="lb">Alamat Instansi</label>
                            <input type="text" class="form-control " id="alamat" name="alamat" />
                          </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Judul Penelitian</label>
                              <textarea type="text" class="form-control" id="judul" name="judul"  ></textarea>
                           </div>
                           <div class="form-group">
                              <label for="nama" class="lb">Tanggal Penelitian</label>
                              <input type="date" class="form-control " id="tgl" name="tgl" />
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button type="submit" class="btn btn-success">» Kirim</button>
                        </div>
                        </form>
                     </div>
                  </div>
               </div>
               <div role="tabpanel" class="tab-pane active" id="status">
                  <div class="panel-body" style="padding-bottom:4vw;">
                     <div id="dataTables-example_wrapper" class="dataTables_wrapper from-inline dt-bootstrap no-footer">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example1">
                           <thead>
                              <tr>
                                 <th style="text-align:center;">No</th>
                                 <th style="text-align:center;">Nama Surat</th>
                                 <th style="text-align:center;">Tanggal Pesan</th>
                                 <th style="text-align:center;">Status</th>
                                 <th style="text-align:center;">Action</th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php
                              $i=1;
                              foreach ($antria as $Hall){ ?>
                              <tr>
                                 <td style="text-align:center;"><?php echo $i; ?></td>
                                 <td style="text-align:center;"><?php echo $Hall['jenis_surat'];?></td>
                                 <td style="text-align:center;"><?php
                                 $tgl = $Hall['tgl_submit'];
                                 echo $tgl = indo($tgl);
                                 ?></td>
                                 <td style="text-align:center;">
                                 <?php
                                    if($Hall['status'] == "1"){ ?>
                                 <a class="btn-xs btn btn-warning">Antri</a>
                                 <?php
                               }else if($Hall['status'] == "2"){ ?>
                                 <a class="btn-xs btn btn-info" >Menunggu</a>
                                 <?php
                               }else { ?>
                                  <a class="btn-xs btn btn-success" >Bisa diambil</a>
                                 <?php
                               }
                                    ?>
                                 </td>
                                 <td>
                                   <?php
                                   if($Hall['status'] == "1"){ ?>
                                     <a class="btn-xs btn btn-danger" href="<?php echo base_url();?>home/delete/<?php echo $Hall['id'];?>" >Delete</a>
                                  <?php  }else { ?>
                                     <button class="btn-xs btn btn-danger" disabled >Delete</button>
                                   <?php }
                                    ?>
                                 </td>
                              </tr>
                              <?php
                              $i++;
                                 }
                                 ?>
                           </tbody>
                        </table>
                        <!--/.table-responsive -->
                     </div>
                  </div>
				<div class="col-xs-12 col-sm-12 col-md-12" style="border-top:1px solid #eee;width:60%; padding-bottom:3vh; padding-top:3vh;padding-left:6vh;">
					<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-3">
						<i style="color:#000;margin-left:2vh;">Keterangan :</i></span>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-3">
						<span class="glyphicon glyphicon-stop" aria-hidden="true" style="color:orange"><i style="color:#000;margin-left:2vh;">Antri</i></span>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-3">
						<span class="glyphicon glyphicon-stop" aria-hidden="true" style="color:blue"><i style="color:#000;margin-left:2vh;">Menunggu</i></span>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-3">
						<span class="glyphicon glyphicon-stop" aria-hidden="true" style="color:green"><i style="color:#000;margin-left:2vh;">Bisa diambil<i></span>
					</div>
					</div>
				</div>
                </div>
               <div role="tabpanel" class="tab-pane" id="profil">
                  <div class="">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form class="" action="<?php echo base_url();?>prin/addSBea" method="post" enctype="multipart/form-data">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:1.5vw; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Surat Rekomendasi</h4>
         </div>
         <div class="modal-body" id="modal_foto">
            <div class="form-group">
               <label for="nama" class="lb">File Pendukung</label>
               <input name="filepen" id="filepen" type="file" accept="application/pdf,application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document" required />
               <a style="color:red">format harus pdf atau word<a>
            </div>
         </div>
         <div class="modal-body" id="modal_foto">
            <div class="form-group">
               <label for="nama" class="lb">Keterangan</label>
               <textarea type="text" class="form-control" id="ket_beasiswa" name="ket_beasiswa" required ></textarea>
            </div>
         </div>
         <div class="modal-body" id="modal_foto" style="text-align:left">
            <div class="form-group">
               <label for="nama" class="lb">Contoh</label>
               <img src="<?php echo base_url();?>assets/images/1.jpg"/>
			   <div class="well well-sm">Lebih jelas silahkan klik link berikut <a href="<?php echo base_url();?>assets/images/ket2.jpg" target="_blank" >lihat contoh</a></div>
            </div>
         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Kirim</button>
         </div>
         </form>
      </div>
   </div>
</div>
