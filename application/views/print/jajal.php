<?php
$fakultascilik1 = str_replace("Fak.","Fakultas",$fakultas);
$fakultascilik = $fakultascilik1;
$fakultas = strtoupper($fakultas);
$fakultas = str_replace("FAK.","FAKULTAS",$fakultas);

 ?>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print/colorbox.css" />
<script>
	$(document).ready(function(){
		// window.print();

	});
</script>
<html>

	<head><title>Surat Rekomendasi</title></head>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print/cetak.css" type="text/css "/>
	<font face="arial" size="11">

	<body>

<table width="650px;" style=margin:auto;>
	<tr>
		<td>
			<div id="header">
					<div id="gambar"><img style="height:10%; width:80%;" src="<?php echo base_url(); ?>assets/images/uin.png"/></div>
					<div id="tulisan_uin"><strong>KEMENTRIAN AGAMA REPUBLIK INDONESIA</strong><br/><span style="font-size:0.9em">UNIVERSITAS ISLAM NEGERI SUNAN AMPEL SURABAYA</span><br/>FAKULTAS EKONOMI DAN BISNIS ISLAM<br/><span style="font-size:0.7em">
					Jl. Jend. A. Yani 117 Surabaya 60237 Telp. 031-8410298 Fax. 031-8413300<br/>
					 Website : <?php echo $singkatan; ?>.uinsby.ac.id E-Mail : <?php echo $singkatan; ?>@uinsby.ac.id</span></div>
				</div>

				<div id="garis"></div>
				<div id="ket"><strong id="surat_keterangan">Surat Rekomendasi</strong><br/>Nomor : B -       /Un.07/8/D/PP.00.9/10/2017</div>
		<br/>
		<br/>
		<br/>
		<div id="">Yang bertanda tangan dibawah ini :</div>
		<br/>
		<div id="">
			<table>
				<tr>
					<td></td>
					<td>Nama</td>
					<td></td>
					<td>:</td>
					<td>Prof. Akh. Muzakki, M.Ag, Grad.Dip.SEA, M.Phil, Ph.D.</td>
				</tr>
				<tr>
					<td></td>
					<td>NIP</td>
					<td></td>
					<td>:</td>
					<td>197402091998031002</td>
				</tr>
				<tr>
					<td></td>
					<td>Jabatan</td>
					<td></td>
					<td>:</td>
					<td><?php echo $jabatan." ".$fakultascilik; ?> UIN Sunan Ampel Surabaya</td>
				</tr>
				<tr>
					<td></td>
					<td>Alamat</td>
					<td></td>
					<td>:</td>
					<td>Jl. A.Yani 117 Surabaya</td>
				</tr>
			</table>
		</div>
		<br/>
		<br/>
		<div id="">Dengan ini Menyatakan bahwa :</div>
		<br/>
		<div id="">
			<table>
				<tr>
					<td></td>
					<td>Nama</td>
					<td></td>
					<td>:</td>
					<td>AYU DIAH KHOIRUN NISA</td>
				</tr>
				<tr>
					<td></td>
					<td>NIM</td>
					<td></td>
					<td>:</td>
					<td>G01214001</td>
				</tr>
				<tr>
					<td></td>
					<td>Prodi</td>
					<td></td>
					<td>:</td>
					<td>Ilmu Ekonomi</td>
				</tr>
				<tr>
					<td></td>
					<td>Semester</td>
					<td></td>
					<td>:</td>
					<td>7</td>
				</tr>
			</table>
		</div>
		<br/>
		<br/>
		<div style="text-align:justify;">adalah benar mahasiswa <?php echo $fakultascilik; ?> UIN Sunan Ampel Surabaya, yang
			bersangkutan direkomendasikan mengikuti <?php print_r ($pengajuan[0]['ket_beasiswa']); ?>
			</div>
		<br/>
		<br/>
		<br/>
		<br/>
		<div class="pull-right" style="text-align:left;" id="id">Surabaya, <?php echo $hari." ".$bulan." ".$tahun ;?>
			<?php
			if ($nodekan == 0) { ?>
				<br/>Dekan<br/>
			<?php
			}else { ?>
				<br/>a.n. Dekan<br/><?php echo $jabatan; ?>
			<?php
			}
			?>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		Prof. Akh. Muzakki, M.Ag, Grad.Dip.SEA, M.Phil, Ph.D.<br/>
		NIP. 197402091998031002

		</div></td>
	</tr>
</table>



	</body>
</font>
</html>
