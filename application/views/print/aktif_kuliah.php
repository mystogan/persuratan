<?php

$fakultascilik1 = str_replace("Fak.","Fakultas",$fakultas);
$fakultascilik2 = $fakultascilik1;
$fakultas = strtoupper($fakultas);
$fakultas = str_replace("FAK.","FAKULTAS",$fakultas);
//echo $fakultas;

if($nobulan >= 1 && $nobulan <= 6){
  $tahunajaran = ($tahun-1)."/".$tahun;
}else {
  $tahunajaran = ($tahun)."/".($tahun+1);
}
$tgllahir = indo($tgllahir);
function indo($date){
  $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
	$tahun = substr($date, 0, 4);
	$bulan = substr($date, 5, 2);
	$tgl   = substr($date, 8, 2);
  // print_r ($BulanIndo[$bulan-1]);
  // echo $bulan;

  $data = $tgl." ".$BulanIndo[$bulan-1]." ".$tahun;
  return $data;
}
 ?>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print/colorbox.css" />
<script>
	$(document).ready(function(){
		window.print();

	});
</script>
<html>

	<head><title>Surat Keterangan Aktif</title></head>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print/cetak.css" type="text/css "/>
<font face="arial" size="11">
	<body>

<table width="650px;" style=margin:auto;>
	<tr>
		<td>


	<div id="header">
			<div id="gambar"><img style="height:10%; width:80%;" src="<?php echo base_url(); ?>assets/images/uin.png"/></div>
      <div id="tulisan_uin"><strong>KEMENTRIAN AGAMA REPUBLIK INDONESIA</strong><br/><span style="font-size:0.9em">UNIVERSITAS ISLAM NEGERI SUNAN AMPEL SURABAYA</span><br/><?php echo $fakultas; ?><br/><span style="font-size:0.7em">
      Jl. Jend. A. Yani 117 Surabaya 60237 Telp. 031-8410298 Fax. 031-8413300<br/> Website : <?php echo $singkatan; ?>.uinsby.ac.id E-Mail : <?php echo $singkatan; ?>@uinsby.ac.id</span></div>
		</div>

		<div id="garis"></div>
		<div id="ket"><strong id="surat_keterangan">Surat Keterangan</strong><br/>Nomor : B -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/Un.07/<?php echo $nomor; ?>/D1/PP.00.9/<?php echo $nobulan."/".$tahun;?></div>
		<br/>
		<br/>
		<br/>
		<div id="">Dekan Fakultas Ilmu Sosial dan Ilmu Politik UIN Sunan Ampel Surabaya, menerangkan bahwa :</div>
		<br/>
		<div id="">
			<table>
				<tr>
					<td></td>
					<td>Nama</td>
					<td></td>
					<td>:</td>
					<td><?php echo $nama;?></td>
				</tr>
				<tr>
					<td></td>
					<td>Tempat, Tanggal lahir</td>
					<td></td>
					<td>:</td>
					<td><?php echo $tmplahir.", ".$tgllahir;?></td>
				</tr>
				<tr>
					<td></td>
					<td>NIM</td>
					<td></td>
					<td>:</td>
					<td><?php echo $nim;?></td>
				</tr>
				<tr>
					<td></td>
					<td>Semester/Prodi</td>
					<td></td>
					<td>:</td>
					<td><?php echo $semestermhs."/".$prodi;?></td>
				</tr>
				<tr>
					<td></td>
					<td>Alamat</td>
					<td></td>
					<td>:</td>
					<td><?php echo $alamat?></td>
				</tr>
			</table>
		</div>
		<br/>
		<br/>
		<div style="text-align:justify;">adalah mahasiswa aktif  <?php echo $fakultascilik2; ?>
      UIN Sunan Ampel Surabaya Semester <?php echo $semestermhs; ?>
      Tahun Akademik <?php echo $tahunajaran; ?> dan tidak sedang menerima beasiswa dari pihak manapun
			</div>
		<br/>
		<br/>
		<div>Demikian surat keterangan ini dibuat untuk dipergunakan sebagaimana mestinya.</div>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<div id="id">Surabaya, <?php echo $hari." ".$bulan." ".$tahun ;?> <br/>a.n. Dekan<br/>Wakil Dekan Bidang Akademik dan Kelembagaan
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<?php echo $wadek1['nama']; ?><br/>
		NIP. <?php echo $wadek1['nip']; ?>

		</div></td>
	</tr>
</table>



	</body>
</font>
</html>
