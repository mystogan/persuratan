<?php
$fakultascilik1 = str_replace("Fak.","Fakultas",$fakultas);
$fakultascilik = $fakultascilik1;
$fakultas = strtoupper($fakultas);
$fakultas = str_replace("FAK.","FAKULTAS",$fakultas);

if($nodekan == 0){
	$kodedekan = "D";
	$jabatan = "Dekan";
	$namaDekan = $dekan['nama'];
	$nipDekan = $dekan['nip'];
}else if($nodekan == 1){
	$kodedekan = "D1";
	$jabatan = "Wakil Dekan Bidang Akademik dan Kelembagaan";
	$namaDekan = $wadek1['nama'];
	$nipDekan = $wadek1['nip'];
}else if($nodekan == 2){
	$kodedekan = "D2";
	$jabatan = "Wakil Dekan 2";
	$namaDekan = $wadek1['nama'];
	$nipDekan = $wadek1['nip'];
}else {
	$kodedekan = "D3";
	$jabatan = "Wakil Dekan Bidang Kemahasiswaan dan Kerjasama";
	$namaDekan = $wadek1['nama'];
	$nipDekan = $wadek1['nip'];
}
 ?>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print/colorbox.css" />
<script>
	$(document).ready(function(){
		window.print();

	});
</script>
<html>

	<head><title>Surat Rekomendasi</title></head>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print/cetak.css" type="text/css "/>
	<font face="arial" size="11">

	<body>

<table width="650px;" style=margin:auto;>
	<tr>
		<td>
			<div id="header">
					<div id="gambar"><img style="height:10%; width:80%;" src="<?php echo base_url(); ?>assets/images/uin.png"/></div>
					<div id="tulisan_uin"><strong>KEMENTRIAN AGAMA REPUBLIK INDONESIA</strong><br/><span style="font-size:0.9em">UNIVERSITAS ISLAM NEGERI SUNAN AMPEL SURABAYA</span><br/><?php echo $fakultas; ?><br/><span style="font-size:0.7em">
					Jl. Jend. A. Yani 117 Surabaya 60237 Telp. 031-8410298 Fax. 031-8413300<br/>
					 Website : <?php echo $singkatan; ?>.uinsby.ac.id E-Mail : <?php echo $singkatan; ?>@uinsby.ac.id</span></div>
				</div>

				<div id="garis"></div>
				<div id="ket"><strong id="surat_keterangan">Surat Rekomendasi</strong><br/>Nomor : B -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/Un.07/<?php echo $nomor; ?>/<?php echo $kodedekan; ?>/PP.00.9/<?php echo $nobulan."/".$tahun;?></div>
		<br/>
		<br/>
		<br/>
		<div id="">Yang bertanda tangan dibawah ini :</div>
		<br/>
		<div id="">
			<table>
				<tr>
					<td></td>
					<td>Nama</td>
					<td></td>
					<td>:</td>
					<td><?php echo $namaDekan;?></td>
				</tr>
				<tr>
					<td></td>
					<td>NIP</td>
					<td></td>
					<td>:</td>
					<td><?php echo $nipDekan;?></td>
				</tr>
				<tr>
					<td></td>
					<td>Jabatan</td>
					<td></td>
					<td>:</td>
					<td><?php echo $jabatan." ".$fakultascilik; ?> UIN Sunan Ampel Surabaya</td>
				</tr>
				<tr>
					<td></td>
					<td>Alamat</td>
					<td></td>
					<td>:</td>
					<td>Jl. A.Yani 117 Surabaya</td>
				</tr>
			</table>
		</div>
		<br/>
		<br/>
		<div id="">Dengan ini Menyatakan bahwa :</div>
		<br/>
		<div id="">
			<table>
				<tr>
					<td></td>
					<td>Nama</td>
					<td></td>
					<td>:</td>
					<td><?php echo $nama;?></td>
				</tr>
				<tr>
					<td></td>
					<td>NIM</td>
					<td></td>
					<td>:</td>
					<td><?php echo $nim;?></td>
				</tr>
				<tr>
					<td></td>
					<td>Prodi</td>
					<td></td>
					<td>:</td>
					<td><?php echo $prodi; ?></td>
				</tr>
				<tr>
					<td></td>
					<td>Semester</td>
					<td></td>
					<td>:</td>
					<td><?php echo $semestermhs; ?></td>
				</tr>
			</table>
		</div>
		<br/>
		<br/>
		<div style="text-align:justify;">adalah benar mahasiswa <?php echo $fakultascilik; ?> UIN Sunan Ampel Surabaya, yang
			bersangkutan direkomendasikan mengikuti <?php print_r ($pengajuan[0]['ket_beasiswa']); ?>
			</div>
		<br/>
		<br/>
		<br/>
		<br/>
		<div style="text-align:center;"id="id">Surabaya, <?php echo $hari." ".$bulan." ".$tahun ;?>
			<?php
			if ($nodekan == 0) { ?>
				<br/>Dekan<br/>
			<?php
			}else { ?>
				<br/>a.n. Dekan<br/><?php echo $jabatan; ?>
			<?php
			}
			?>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<?php
		echo $namaDekan; ?><br/>
		NIP. <?php echo $nipDekan; ?>

		</div></td>
	</tr>
</table>



	</body>
</font>
</html>
