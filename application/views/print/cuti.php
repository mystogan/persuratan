<?php

$fakultas = strtoupper($fakultas);
$fakultas = str_replace("FAK.","FAKULTAS",$fakultas);
//echo $fakultas;

if($nobulan >= 1 && $nobulan <= 6){
  $tahunajaran = ($tahun-1)."/".$tahun;
	$sem = "Genap";
}else {
  $tahunajaran = ($tahun)."/".($tahun+1);
	$sem = "Gasal";
}
	$kodedekan = "D";
	$jabatan = "Dekan";
	$namaDekan = $dekan['nama'];
	$nipDekan = $dekan['nip'];

?>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print/colorbox.css" />
<script>
	$(document).ready(function(){
		window.print();

	});
</script>
<html>

	<head><title>Surat Keterangan</title></head>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print/cetak.css" type="text/css "/>
<font face="arial" size="11">
	<body>

<table width="650px;" style=margin:auto;>
	<tr>
		<td>



		<br/>
		<br/>
		<div id="">Perihal : <strong>Permohonan Cuti Studi</strong>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	Surabaya, <?php echo $hari." ".$bulan." ".$tahun ;?></div>
		<br/>
		<br/>
		<br/>
		<div id="">Kepada Yth,</div>
		<div id=""><strong>Rektor UIN Sunan Ampel Surabaya</strong></div>
		<div id=""><strong>Jl. A. Yani 117</strong></div>
		<div id="">di &nbsp;<u>Surabaya</u></div>
		<br/>
		<br/>
		<br/>
		<div id=""><i>Assalamu'alaikum Wr. Wb.</i></div><br>
		<div id="">Yang bertanda tangan di bawah ini :</div>
		<br/>
		<div id="">
			<table>
				<tr>
					<td>1. </td>
					<td>Nama</td>
					<td></td>
					<td>:</td>
					<td><?php echo $nama;?></td>
				</tr>
				<tr>
					<td>2. </td>
					<td>NIM</td>
					<td></td>
					<td>:</td>
					<td><?php echo $nim?></td>
				</tr>
				<tr>
					<td>3. </td>
					<td>Tempat/ Tanggal Lahir</td>
					<td></td>
					<td>:</td>
					<td><?php echo $tmplahir.", ".$tgllahir;?></td>
				</tr>
				<tr>
					<td>4. </td>
					<td>Program Studi</td>
					<td></td>
					<td>:</td>
					<td><?php echo $prodi;?></td>
				</tr>
				<tr>
					<td>5. </td>
					<td>Alamat</td>
					<td></td>
					<td>:</td>
					<td><?php echo $alamat;?></td>
				</tr>
			</table>
		</div>
		<br/>
		<br/>
		<div style="text-align:justify;">Dengan ini saya mohon kepada Bapak Rektor, agar dapat diberi izin cuti studi yang ke pertama pada
			Semester <?php echo $sem; ?> Tahun Akademik <?php echo $tahunajaran; ?>, dengan alasan bekerja.
			</div>
			<p>
				<i>Wassalamu'alaikum Wr. Wb.</i>
			</p>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<div class="pull-right" style="text-align:left;">
		<div id="id2">Mengetahui :
			<br/><?php echo $jabatan; ?>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<?php echo $namaDekan; ?><br/>
		NIP. <?php echo $nipDekan; ?>

		</div>
		<div id="id">Pemohon
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<?php echo $nama; ?><br/>
		NIM. <?php echo $nim; ?>

		</div>
		</div>
		<br/>
		<br/>
		<div>Tindasan disampaikan kepada Yth :	</div>
		<div>1. Wali Studi yang bersangkutan	</div>
		<div>2. Kaprodi Prodi yang Bersangkutan	</div>

	</td>
	</tr>
</table>



	</body>
</font>
</html>
