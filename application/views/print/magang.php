<?php
$fakultascilik1 = str_replace("Fak.","Fakultas",$fakultas);
$fakultascilik = $fakultascilik1;
$fakultas = strtoupper($fakultas);
$fakultas = str_replace("FAK.","FAKULTAS",$fakultas);
//echo $fakultas;
// echo $pengajuan[0]['tgl_akhir'];
$datetime1 = new DateTime($pengajuan[0]['tgl_penelitian']);
$datetime2 = new DateTime($pengajuan[0]['tgl_akhir']);
$difference = $datetime2->diff($datetime1);
$selama_hari = $difference->days;

$tgl_awal = indo($pengajuan[0]['tgl_penelitian']);
$tgl_akhir = indo($pengajuan[0]['tgl_akhir']);

if($nobulan >= 1 && $nobulan <= 6){
  $tahunajaran = ($tahun-1)."/".$tahun;
	$sem = "Genap";
}else {
  $tahunajaran = ($tahun)."/".($tahun+1);
	$sem = "Gasal";
}
if($nodekan == 0){
	$kodedekan = "D";
	$jabatan = "Dekan";
	$namaDekan = $dekan['nama'];
	$nipDekan = $dekan['nip'];
}else if($nodekan == 1){
	$kodedekan = "D1";
	$jabatan = "Wakil Dekan Bidang Akademik dan Kelembagaan";
	$namaDekan = $wadek1['nama'];
	$nipDekan = $wadek1['nip'];
}else if($nodekan == 2){
	$kodedekan = "D2";
	$jabatan = "Wakil Dekan Bidang Administrasi Umum Perencanaan dan Keuangan";
	$namaDekan = $wadek1['nama'];
	$nipDekan = $wadek1['nip'];
}else {
	$kodedekan = "D3";
	$jabatan = "Wakil Dekan Bidang Kemahasiswaan dan Kerjasama";
	$namaDekan = $wadek1['nama'];
	$nipDekan = $wadek1['nip'];
}


function indo($date){
  $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

	$tahun = substr($date, 0, 4);
	$bulan = substr($date, 5, 2);
	$tgl   = substr($date, 8, 2);

  $data = $tgl."-".$bulan."-".$tahun;
  return $data;
}
 ?>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print/colorbox.css" />
<script>
	$(document).ready(function(){
		// window.print();

	});
</script>
<html>

	<head><title>Surat Keterangan</title></head>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print/cetak.css" type="text/css "/>
<font face="arial" size="11">
	<body>

<table width="650px;" style=margin:auto;>
	<tr>
		<td>
      <div id="header">
          <div id="gambar"><img style="height:10%; width:80%;" src="<?php echo base_url(); ?>assets/images/uin.png"/></div>
          <div id="tulisan_uin"><strong>KEMENTRIAN AGAMA REPUBLIK INDONESIA</strong><br/><span style="font-size:0.8em">UNIVERSITAS ISLAM NEGERI SUNAN AMPEL SURABAYA</span><br/><?php echo $fakultas; ?><br/><span style="font-size:0.7em">
          Jl. Jend. A. Yani 117 Surabaya 60237 Telp. 031-8410298 Fax. 031-8413300<br/>
           Website : <?php echo $singkatan; ?>.uinsby.ac.id E-Mail : <?php echo $singkatan; ?>@uinsby.ac.id</span></div>
        </div>

        <div id="garis"></div>
        <br/>
        <br/>
<table>
  <tr>
    <td>Nomor</td>
    <td>:</td>
    <td>B -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/Un.07/<?php echo $nomor; ?>/<?php echo $kodedekan; ?>/PP.00.9/<?php echo $nobulan."/".$tahun;?></td>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Surabaya, <?php echo $hari." ".$bulan." ".$tahun ;?></td>
    <td></td>
  </tr>
  <tr>
    <td>Lampiran</td>
    <td>:</td>
    <td> 1 (Satu)Lembar</td>
  </tr>
  <tr>
    <td>Perihal</td>
    <td>:</td>
    <td><strong> Permohonan Kerjasama Praktik</strong></td>
    <td><strong>  Kuliah Lapangan</strong></td>
  </tr>
</table>

		<br/>
		<br/>
		<div id="">Kepada Yth,</div>
    <div id=""><strong>Pimpinan <?php print_r ($pengajuan[0]['ket_instansi']);  ?></strong></div>
		<div id=""><strong><?php print_r ($pengajuan[0]['alamat_instansi']);  ?></strong></div>
		<!-- <div id="">di &nbsp;<u>Surabaya</u></div> -->
		<br/>
		<br/>
		<br/>
		<div id=""><i><strong>Assalamu'alaikum Wr. Wb.</strong></i></div><br>
		<div id="" style="text-align:justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      Sehubung dengan program peningkatan kompetensi dan ketrampilan mahasiswa Prodi <?php echo $prodi; ?>
      <?php echo $fakultascilik; ?> Universitas Islam Negeri Sunan Ampel Surabaya, maka dengan ini kami
      bermaksud mengajukan permohonan untuk menjalin kerjasama dalam bentuk bimbingan dan tempat
      pelaksanaan praktik di lembaga yang Bapak/Ibu pimpin. Adapun pelaksanaan kegiatan tersebut
      selama <?php echo $selama_hari; ?> Hari mulai tanggal <?php echo $tgl_awal; ?> sampai <?php echo $tgl_akhir; ?>  dengan nama mahasiswa sebagaimana terlampir.</div>
		<div style="text-align:justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      Besar Harapan kami kerjasama ini terjalin secara baik, atas perkenan dan dukungan Bapak/Ibu disampaikan terima kasih

			</div>
			<p>
				<i><strong>Wassalamu'alaikum Wr. Wb.</strong></i>
			</p>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<div>
		<div id="id">Mengetahui :
			<br/>a.n. Dekan<br/><?php echo $jabatan; ?>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<?php echo $namaDekan; ?><br/>
		NIP. <?php echo $nipDekan; ?>

		</div>
		</div>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
    <table style="border: 1px solid black; width:100%;">
      <tr style="border: 1px solid black;">
        <th style="border: 1px solid black;" >No</th>
        <th style="border: 1px solid black; width : 45%;">Nama</th>
        <th style="border: 1px solid black;">NIM</th>
        <th style="border: 1px solid black;">Prodi</th>
      </tr>
      <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;">1</td>
        <td style="border: 1px solid black;"><?php echo $nama; ?></td>
        <td style="border: 1px solid black;"><?php echo $nim; ?></td>
        <td style="border: 1px solid black;"><?php echo $prodi; ?></td>
      </tr>

      <?php
        for ($i=2; $i < 16 ; $i++) {
          ?>
          <tr style="border: 1px solid black;">
            <td style="border: 1px solid black;"><?php echo $i; ?></td>
            <td style="border: 1px solid black;"></td>
            <td style="border: 1px solid black;"></td>
            <td style="border: 1px solid black;"></td>
          </tr>

      <?php
        }
       ?>
    </table>


	</td>
	</tr>
</table>


	</body>
</font>
</html>
