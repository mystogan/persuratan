<?php
$fakultascilik1 = str_replace("Fak.","Fakultas",$fakultas);
$fakultascilik2 = $fakultascilik1;
$fakultas = strtoupper($fakultas);
$fakultas = str_replace("FAK.","FAKULTAS",$fakultas);
//echo $fakultas;

if($nobulan >= 1 && $nobulan <= 6){
  $tahunajaran = ($tahun-1)."/".$tahun;
	$sem = "Genap";
}else {
  $tahunajaran = ($tahun)."/".($tahun+1);
	$sem = "Gasal";
}
$tgl = indo($pengajuan[0]['tgl_penelitian']);

if($nodekan == 0){
	$kodedekan = "D";
	$jabatan = "Dekan";
	$namaDekan = $dekan['nama'];
	$nipDekan = $dekan['nip'];
}else if($nodekan == 1){
	$kodedekan = "D1";
	$jabatan = "Wakil Dekan Bidang Akademik dan Kelembagaan";
	$namaDekan = $wadek1['nama'];
	$nipDekan = $wadek1['nip'];
}else if($nodekan == 2){
	$kodedekan = "D2";
	$jabatan = "Wakil Dekan Bidang Administrasi Umum Perencanaan dan Keuangan";
	$namaDekan = $wadek1['nama'];
	$nipDekan = $wadek1['nip'];
}else {
	$kodedekan = "D3";
	$jabatan = "Wakil Dekan Bidang Kemahasiswaan dan Kerjasama";
	$namaDekan = $wadek1['nama'];
	$nipDekan = $wadek1['nip'];
}

function indo($date){
  $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

	$tahun = substr($date, 0, 4);
	$bulan = substr($date, 5, 2);
	$tgl   = substr($date, 8, 2);

  $data = $tgl."-".$bulan."-".$tahun;
  return $data;
}
 ?>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print/colorbox.css" />
<script>
	$(document).ready(function(){
		// window.print();

	});
</script>
<html>

	<head><title>Surat Keterangan</title></head>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print/cetak.css" type="text/css "/>
  <font face="arial" size="11">
	<body>

<table width="650px;" style=margin:auto;>
	<tr>
		<td>
      <div id="header">
          <div id="gambar"><img style="height:10%; width:80%;" src="<?php echo base_url(); ?>assets/images/uin.png"/></div>
          <div id="tulisan_uin"><strong>KEMENTRIAN AGAMA REPUBLIK INDONESIA</strong><br/><span style="font-size:0.8em">UNIVERSITAS ISLAM NEGERI SUNAN AMPEL SURABAYA</span><br/><?php echo $fakultas; ?><br/><span style="font-size:0.7em">
          Jl. Jend. A. Yani 117 Surabaya 60237 Telp. 031-8410298 Fax. 031-8413300<br/>
           Website : <?php echo $singkatan; ?>.uinsby.ac.id E-Mail : <?php echo $singkatan; ?>@uinsby.ac.id</span></div>
        </div>

        <div id="garis"></div>
        <br/>
        <br/>
<table>
  <tr>
    <td>Nomor</td>
    <td>:</td>
    <td>B -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/Un.07/<?php echo $nomor; ?>/<?php echo $kodedekan; ?>/PP.00.9/<?php echo $nobulan."/".$tahun;?></td>
    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    <td>Surabaya, <?php echo $hari." ".$bulan." ".$tahun ;?></td>
  </tr>
  <tr>
    <td>Lampiran</td>
    <td>:</td>
    <td> -</td>
  </tr>
  <tr>
    <td>Perihal</td>
    <td>:</td>
    <td><strong> Permohonan Izin Pra Penelitian</strong></td>
  </tr>
</table>

		<br/>
		<br/>
		<div id="">Kepada Yth,</div>
		<div id=""><strong>Pimpinan <?php print_r ($pengajuan[0]['ket_instansi']);  ?></strong></div>
		<div id=""><strong><?php print_r ($pengajuan[0]['alamat_instansi']);  ?></strong></div>
		<!-- <div id="">di &nbsp;<u>Surabaya</u></div> -->
		<br/>
		<br/>
		<br/>
		<div id=""><i><strong>Assalamu'alaikum Wr. Wb.</strong></i></div><br>
		<div id="" style="text-align:justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      Sehubungan dengan program peningkatan kompetensi dan ketrampilan mahasiswa <?php echo $fakultascilik2; ?>
      Universitas Islam Negeri Sunan Ampel Surabaya bidang
      penelitian, bersama ini dekan menyampaikan bahwa mahasiswa dengan identitas sebagai berikut .</div>
      <br/>
  		<div id="">
  			<table>
  				<tr>
  					<td></td>
  					<td>Nama</td>
  					<td></td>
  					<td>:</td>
  					<td><?php echo $nama;?></td>
  				</tr>
  				<tr>
  					<td></td>
  					<td>NIM</td>
  					<td></td>
  					<td>:</td>
  					<td><?php echo $nim; ?> </td>
  				</tr>
  				<tr>
  					<td></td>
  					<td>Semester/Prodi</td>
  					<td></td>
  					<td>:</td>
  					<td><?php echo $semestermhs."/".$prodi;?></td>
  				</tr>
  			</table>
  		</div>
  		<br/>

    <div style="text-align:justify;">
      bermaksud melakukan pra penelitian pada tanggal <?php print_r ($tgl);  ?> dengan judul "<strong><?php print_r ($pengajuan[0]['judul_penelitian']);  ?> </strong>". Oleh karena
      itu, kami mohon kepada pimpinan <?php print_r ($pengajuan[0]['ket_instansi']);  ?>  untuk berkenan memberikan izin, demi kelancaran penelitian yang bersangkutan.
			</div>
    <div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      Demikian permohonan izin ini, dan atas kerjasamanya kami sampaikan terimakasih.
			</div>
			<p>
				<i><strong>Wassalamu'alaikum Wr. Wb.</strong></i>
			</p>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<div>
		<div id="id">Mengetahui :
			<br/>a.n. Dekan<br/><?php echo $jabatan; ?>
		<br/>
		<br/>
		<br/>
		<br/>
		<br/>
		<?php echo $namaDekan; ?><br/>
		NIP. <?php echo $nipDekan; ?>

		</div>
		</div>

	</td>
	</tr>
</table>



	</body>
</font>
</html>
