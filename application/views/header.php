<html>
  <head><meta charset="utf-8">
    <link href='<?php echo base_url();?>assets/images/logo.png' rel='SHORTCUT ICON'/>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Persuratan</title>
  	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/reset.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/form.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-table.css" type="text/css "/>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/headerfooter.css" type="text/css "/>

	<!-- DataTables CSS -->
	<link href="<?php echo base_url();?>assets/css/dataTables.bootstrap.css" rel="stylesheet">

	<!-- DataTables Responsive CSS -->
	<link href="<?php echo base_url();?>assets/css/dataTables.responsive.css" rel="stylesheet">

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js" type="text/javascript" ></script>
	<script src="<?php echo base_url();?>assets/js/dataTables.bootstrap.min.js" type="text/javascript" ></script>
	<script src="<?php echo base_url();?>assets/js/dataTables.responsive.js" type="text/javascript" ></script>
	<!-- CKeditor CSS -->
	<script src="<?php echo base_url();?>assets/js/ckeditor.js"></script>
	<script src="<?php echo base_url();?>assets/js/proses.js"></script>

	<script>
  base_url = "<?php echo base_url();?>";
	$(document).ready(function() {
		$('#dataTables-example').DataTable({
				responsive: true
		});
	});
	$(document).ready(function() {
		$('#dataTables-example1').DataTable({
				responsive: true
		});
	});
	$(document).ready(function() {
		$('#dataTables-example2').DataTable({
				responsive: true
		});
	});
	$(document).ready(function() {
		$('#dataTables-example3').DataTable({
				responsive: true
		});
	});

  setTimeout(function(){
   window.location.reload(1);
 }, 120000);
	</script>
  </head>

  <style>

</style>

<body>
	<!--<div class="atas">
		<img src="<?php echo base_url();?>assets/images/logo.png"/>
	</div>-->

<!--<div class="header">
<div class="container">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="row">
			<div class="judul">Persuratan</div>
		</div>
	</div>
</div>
</div>-->
