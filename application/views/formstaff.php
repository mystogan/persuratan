<?php
function indo($date){
  $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
	$tahun = substr($date, 0, 4);
	$bulan = substr($date, 5, 2);
	$tgl   = substr($date, 8, 2);
	$jam  = substr($date, 11, 2);
	$menit  = substr($date, 14, 2);
  // print_r ($BulanIndo[$bulan-1]);
  //  echo $jam."-".$menit;

  $data = $tgl." ".$BulanIndo[$bulan-1]." ".$tahun." ".$jam.":".$menit;
  return $data;
}

 ?>

<div class="contentmenu">
<div class="container">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="row">
			<div class="menu">
				<ul class="nav nav-tabs tab1" style="" role="tablist">
              <li role="presentation" class="active" ><a href="#surat" aria-controls="surat" role="tab" data-toggle="tab"><img title="Permintaan Surat" style="width:3vh;margin-right:1vh;" src="<?php echo base_url();?>assets/images/1.png"/>Permintaan Surat</a></li>
              <li role="presentation"><a href="#Eksekusi" aria-controls="Eksekusi" role="tab" data-toggle="tab"><img title="Riwayat" style="width:3vh;margin-right:1vh;" src="<?php echo base_url();?>assets/images/2.png"/>Notifikasi</a></li>
              <li role="presentation"><a href="#Riwayat" aria-controls="Riwayat" role="tab" data-toggle="tab"><img title="Riwayat" style="width:3vh;margin-right:1vh;" src="<?php echo base_url();?>assets/images/clock_history.png"/>Riwayat</a></li>
              <!--<li role="presentation"><a href="#Pengaturan" aria-controls="Pengaturan" role="tab" data-toggle="tab"><img title="Pengaturan" style="width:3vh;margin-right:1vh;" src="<?php echo base_url();?>assets/images/5.png"/>Pengaturan</a></li>-->
				<li role="presentation"><a href="<?php echo base_url();?>admin/logout"><img title="Status" style="width:3vh;margin-right:1vh;" src="<?php echo base_url();?>assets/images/logout.png"/>Keluar</a></li>
		  </ul>
			</div>
		</div>
	</div>
</div>
</div>
<div class="content">
<div class="container">
	<div class="col-xs-12 col-sm-12 col-md-12">
		<div class="row">
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="surat">
						<div class="panel-body">

		                            <div id="dataTables-example_wrapper" class="dataTables_wrapper from-inline dt-bootstrap no-footer">

		                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
		                                <thead>
		                                    <tr>
		                                        <th style="text-align:center;">NIM</th>
		                                        <th style="text-align:center;">Nama Mahasiswa</th>
		                                        <th style="text-align:center;">Prodi</th>
		                                        <th style="text-align:center;">Surat Terkait</th>
		                                        <th style="text-align:center;">Tanggal Pesan</th>
		                                        <th style="text-align:center;">Status</th>
		                                        <th style="text-align:center;">Print</th>
		                                    </tr>
		                                </thead>
		                                <tbody>
																			<?php
																			foreach ($antri as $Hantri ) { ?>

																				<tr>
																					<td style="text-align:center;"><?php echo $Hantri['nim'] ?></td>
																					<td style="text-align:center;"><?php echo $Hantri['nama'] ?></td>
																					<td style="text-align:center;"><?php echo $Hantri['prodi'] ?></td>
																					<td style="text-align:center;"><?php echo $Hantri['jenis_surat']; ?></td>
																					<td style="text-align:center;"><?php
																					$tgl = $Hantri['tgl_submit'];
																					echo $tgl = indo($tgl);
																					?></td>
																					<td style="text-align:center;"><a class="btn-xs btn btn-warning">Belum di Print</a></td>
																					<td style="text-align:center;">
																						<?php
																						if($Hantri['kode_surat'] == 2 || $Hantri['kode_surat'] == 6 || $Hantri['kode_surat'] == 5 ){ ?>
																								<div data-toggle="modal" data-target="#myModal2" ><a class="btn btn-default" type="button" href="javascript:setnim('<?php echo $Hantri['nim']; ?>','<?php echo $Hantri['id']; ?>','<?php echo $Hantri['kode_surat']; ?>');">Print</a></div>
																						<?php
																						}else {
																						?>
																						<form class="" action="<?php echo base_url(); ?>prin" method="post">
																							<input type="hidden" name="id_surat" value="<?php echo $Hantri['id']; ?>" >
																							<input type="hidden" name="kode_surat" value="<?php echo $Hantri['kode_surat']; ?>" >
																							<input type="hidden" name="nim" value="<?php echo $Hantri['nim']; ?>" >
																							<button type="submit" class="btn btn-default">Print</button>
																						</form>
																						<?php
																						}
																						 ?>
																					</td>
																				</tr>

																			<?php
																			}
																			 ?>


		                                </tbody>
		                            </table>

		                            <!-- /.table-responsive -->
		                            </div>
		                        </div>
					</div>
					<div role="tabpanel" class="tab-pane" id="Eksekusi">
						<div class="panel-body">

		                            <div id="dataTables-example_wrapper" class="dataTables_wrapper from-inline dt-bootstrap no-footer">

		                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example2">
		                                <thead>
		                                    <tr>
		                                        <th style="text-align:center;">NIM</th>
		                                        <th style="text-align:center;">Nama Mahasiswa</th>
		                                        <th style="text-align:center;">Prodi</th>
		                                        <th style="text-align:center;">Surat</th>
		                                        <th style="text-align:center;">Tanggal Pesan</th>
		                                        <th style="text-align:center;">Lampiran</th>
		                                        <th style="text-align:center;">Status</th>
		                                        <th style="text-align:center;"></th>
		                                    </tr>
		                                </thead>
		                                <tbody>
																			<?php
																			foreach ($menunggu as $Hmenunggu ) { ?>
																				<tr>
																					<td style="text-align:center;">
																						<?php
																						if($Hmenunggu['kode_surat'] == 2){ ?>
																								<div data-toggle="modal" data-target="#myModal2" ><a class="btn" type="button" href="javascript:setnim('<?php echo $Hmenunggu['nim']; ?>','<?php echo $Hmenunggu['id']; ?>','<?php echo $Hmenunggu['kode_surat']; ?>');"><?php echo $Hmenunggu['nim']; ?></a></div>
																						<?php
																						}else {
																						?>
																						<form class="" action="<?php echo base_url(); ?>prin" method="post">
																							<input type="hidden" name="id_surat" value="<?php echo $Hmenunggu['id']; ?>" >
																							<input type="hidden" name="kode_surat" value="<?php echo $Hmenunggu['kode_surat']; ?>" >
																							<input type="hidden" name="nim" value="<?php echo $Hmenunggu['nim']; ?>" >
																							<button type="submit" class="btn"><?php echo $Hmenunggu['nim']; ?></button>
																						</form>
																						<?php
																						}
																						 ?>
																					</td>
																					<td style="text-align:center;"><?php echo $Hmenunggu['nama']; ?></td>
																					<td style="text-align:center;"><?php echo $Hmenunggu['prodi']; ?></td>
																					<td style="text-align:center;"><?php echo $Hmenunggu['jenis_surat']; ?></td>
																					<td style="text-align:center;"><?php
																					$tgl = $Hmenunggu['tgl_submit'];
																					echo $tgl = indo($tgl);
																					?></td>
																					<td style="text-align:center;">
																						<?php
																						if($Hmenunggu['file'] == null){
																							echo "-";
																						}else { ?>
																							<a href="<?php echo base_url()."assets/foto/".$Hmenunggu['file']; ?>"><?php echo $Hmenunggu['file']; ?></a>
																						<?php
																						}
																						 ?>
																					</td>
																					<td style="text-align:center;"><a class="btn-xs btn btn-info" >Menunggu</a></td>
																					<td style="text-align:center;"><a class="btn btn-success" type='button' href="<?php echo base_url();?>home/selesai/<?php echo $Hmenunggu['id']; ?>" >Selesai</a></td>
																				</tr>

																			<?php
																			}
																			?>

		                                </tbody>
		                            </table>

		                            <!-- /.table-responsive -->
		                            </div>
		                        </div>
					</div>
					<div role="tabpanel" class="tab-pane" id="Riwayat">
						<div class="panel-body">

		                            <div id="dataTables-example_wrapper" class="dataTables_wrapper from-inline dt-bootstrap no-footer">

		                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example3">
		                                <thead>
		                                    <tr>
		                                        <th style="text-align:center;">NIM</th>
		                                        <th style="text-align:center;">Nama Mahasiswa</th>
		                                        <th style="text-align:center;">Prodi</th>
		                                        <th style="text-align:center;">Jenis Surat</th>
		                                        <th style="text-align:center;">Tanggal Pesan</th>
		                                        <th style="text-align:center;">Status</th>
		                                    </tr>
		                                </thead>
		                                <tbody>
																			<?php foreach ($all as $Hall){ ?>
																				<tr>
		                                        <td style="text-align:center;"><?php echo $Hall['nim'];?></td>
		                                        <td style="text-align:center;"><?php echo $Hall['nama'];?></td>
		                                        <td style="text-align:center;"><?php echo $Hall['prodi'];?></td>
		                                        <td style="text-align:center;"><?php echo $Hall['jenis_surat'];?></td>
		                                        <td style="text-align:center;"><?php

																						$tgl = $Hall['tgl_submit'];
																						echo $tgl = indo($tgl);
																						?></td>
																						<td style="text-align:center;">
					                                  <?php
					                                     if($Hall['status'] == "1"){ ?>
							                                  <a class="btn-xs btn btn-warning">Belum diprint</a>
							                                  <?php
							                                }else if($Hall['status'] == "2"){ ?>
							                                  <a class="btn-xs btn btn-info" >Menunggu</a>
							                                  <?php
							                                }else { ?>
							                                   <a class="btn-xs btn btn-success" >Bisa diambil</a>
							                                  <?php
							                                }
					                                     ?>
					                                  </td>
		                                    </tr>
																			<?php
																			}
																			?>

		                                </tbody>
		                            </table>

		                            <!-- /.table-responsive -->
		                            </div>
		                        </div>
					</div>
					<!--<div role="tabpanel" class="tab-pane" id="Pengaturan">
						ya
					</div>-->
				</div>
		</div>
	</div>
</div>
</div>

<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
				<form class="" action="<?php echo base_url(); ?>prin" method="post">
					<input type="hidden" name="kode_surat" id="kode_surat" >
					<input type="hidden" name="nim" id="nimmhs" >
					<input type="hidden" name="id_surat" id="id_surat"  >
				 <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 style="font-size:3vh; text-align:center;padding-top:1vw;" class="modal-title" id="myModalLabel">Surat Rekomendasi</h4>
         </div>
         <div class="modal-body" id="modal_foto">
            <div class="form-group">
               <label for="nama" class="lb">Keterangan</label>
               <select class="form-control" name="dekan">
               	<option value="0">Dekan</option>
               	<option value="1">Wakil Dekan 1</option>
               	<option value="2">Wakil Dekan 2</option>
               	<option value="3">Wakil Dekan 3</option>
               </select>
            </div>
         </div>
         <div class="modal-footer">
            <button class="btn btn-success" type="submit">» Kirim</button>
         </div>
         </form>
      </div>
   </div>
</div>
