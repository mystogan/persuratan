<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	function __construct(){
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->database();
		$this->load->model('Home_model');
		$this->load->helper(array('form','url','file','download'));
		//error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		 $nim = $this->session->userdata('nim');
		if($nim == null){
			header("location:".base_url()."login");
		}
	}

	public function index(){
		$kodefak = $this->session->userdata('kodefak');
		$nim = $this->session->userdata('nim');
		$this->getFakultas($kodefak);
		$data['profil'] = $this->getProfil($nim);
		$data["antria"] = $this->Home_model->getantri($nim);
		print_r ($data['profil']);
		$this->load->view('header');
		$this->load->view('form',$data);
		$this->load->view('footer');
	}


	public function getFakultas($kode){
		$conn = pg_connect("host=180.250.165.150 port=5432 dbname=iainmigrasi user=iain password='ampelakademik!3'");
		//$result = pg_prepare($conn, "my_query", 'SELECT * FROM gate.sc_user WHERE username = $1 and password = $2
		$result = pg_prepare($conn, "my_query", "select namaunit from gate.ms_unit where kodeunit = $1");
		$result = pg_execute($conn, "my_query", array($kode));
		while ($row = pg_fetch_assoc($result)){
			$fak = $row['namaunit'];
		}
		$this->session->set_userdata('fakultas',$fak);
		//return $fak;
	}

	public function getProfil($nim){
		//  echo 'get profil mas '.$nim;
		$conn = pg_connect("host=180.250.165.150 port=5432 dbname=iainmigrasi user=iain password='ampelakademik!3'");
		//$result = pg_prepare($conn, "my_query", 'SELECT * FROM gate.sc_user WHERE username = $1 and password = $2
		$result = pg_prepare($conn, "my_query2", "select semestermhs , alamat , tgllahir , ipk
																						from akademik.ms_mahasiswa
																						where nim = $1");
		$result = pg_execute($conn, "my_query2", array($nim));
		while ($row = pg_fetch_assoc($result)){
			$semestermhs = $row['semestermhs'];
			$alamat = $row['alamat'];
			$tgllahir = $row['tgllahir'];
			$ipk = $row['ipk'];
		}

		//return $fak;
	}
	public function profil(){
		// $nim = "H96214021";
		$data["antri"] = $this->Home_model->getantri($nim);
		$this->load->view('header');
		$this->load->view('form',$data);
		$this->load->view('footer');
	}
	public function delete($id){
		$data = array(
				 'status' => "0"
			);
		$this->Home_model->hapus($id,$data);
		header("location:".base_url()."home");
	}
	public function selesai($id){
		$data = array(
				 'status' => "3"
			);
		$this->Home_model->hapus($id,$data);
		header("location:".base_url()."admin");
	}

	public function rekom(){
		$this->load->view('header');
		$this->load->view('rekom');
		$this->load->view('footer');
	}
	public function surat(){
		$this->load->view('print/magang');
	}

}
