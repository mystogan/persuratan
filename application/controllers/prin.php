<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Prin extends CI_Controller {
	function __construct(){
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->database();
		$this->load->model('Home_model');
		$this->load->helper(array('form','url','file','download'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

	}

	public function index(){
		$id_surat = $this->input->post('id_surat');
		$kode = $this->input->post('kode_surat');
		$nim = $this->input->post('nim');
		$nodekan = $this->input->post('dekan');
		if($kode == 1){
			$surat = "aktif_kuliah";
		}else if($kode == 2){
			$surat = "rekom";
		}else if($kode == 3){
			$surat = "cuti";
		}else if($kode == 4){
			$surat = "aktiftok";
		}else if($kode == 5){
			$surat = "magang";
		}else if($kode == 6){
			$surat = "penelitian";
		}
		$kodefak = $this->session->userdata('kodefak');
		$day = date("d");
		$month = date("m");
		$year = date("Y");
		$bulan = $this->getBulan($month);
		$datas = $this->getProfil($nim);
		$dataAjuan = $this->Home_model->getAjuan($id_surat);
		$datass = $this->Home_model->getNomor($datas['kodefakultas']);
		$datafakultas = $this->getfakultas($datas['kodefakultas']);
		$dekan = $this->getdekan($datafakultas['dekan'],$datas['kodefakultas']);
		$wadek1 = $this->getdekan($datafakultas['wadek1'],$datas['kodefakultas']);
		$wadek2 = $this->getdekan($datafakultas['wadek2'],$datas['kodefakultas']);
		$wadek3 = $this->getdekan($datafakultas['wadek3'],$datas['kodefakultas']);
		// print_r ($datas);
		$data = array(
				 'nim' => $datas['nim'] ,
				 'nama' => $datas['nama'] ,
				 'kodeunit' => $datas['kodeunit'] ,
				 'semestermhs' => $datas['semester'] ,
				 'alamat' => $datas['alamat'] ,
				 'tgllahir' => $datas['tgllahir'] ,
				 'tmplahir' => $datas['tmplahir'] ,
				 'prodi' => $datas['prodi'] ,
				 'ipk' => $datas['ipk'] ,
				 'kodefakultas' => $datas['kodefakultas'] ,
				 'fakultas' => $datafakultas['fakultas'] ,
				 'hari' => $day ,
				 'nobulan' => $month ,
				 'bulan' => $bulan ,
				 'nomor' => $datass[0]['nomor'] ,
				 'tahun' => $year,
				 'dekan' => $dekan,
				 'wadek1' => $wadek1,
				 'wadek2' => $wadek2,
				 'wadek3' => $wadek3,
				 'nodekan' => $nodekan,
				 'singkatan' => $datass[0]['singkatan'],
				 'pengajuan' => $dataAjuan
			);

			$dataUpdate = array(
					 'status' => '2'
				);
			//echo $id_surat;

		// print_r($data);
		$this->Home_model->hapus($id_surat,$dataUpdate);
		$this->load->view('print/'.$surat,$data);
	}

	public function addSBea(){
		date_default_timezone_set("Asia/Jakarta");
		$surat = "Surat Rekomendasi Beasiswa";
		$nim = $this->session->userdata('nim');
		$nama = $this->session->userdata('nama');
		$prodi = $this->session->userdata('prodi');
		$ket_beasiswa = $this->input->post('ket_beasiswa');
		$filep = $_FILES["filepen"]["name"];
		$namafile = $this->upload($filep,"filepen");
		$now = date("Y-m-d H:i:s");
		$data = array(
				 'nim' => $nim ,
				 'kode_surat' => '2' ,
				 'ket_beasiswa' => $ket_beasiswa ,
				 'jenis_surat' => $surat ,
				 'nama' => $nama ,
				 'prodi' => $prodi ,
				 'tgl_submit' => $now ,
				 'file' => $namafile ,
				 'status' => "1"
			);
		$cek = $this->Home_model->cekantri($nim,2);
		if($cek == 1){
			echo "<script>alert ('Maaf Anda Sudah Pesan !!!');window.location.href = '".base_url()."home';</script>";
		}else {
			$this->Home_model->savePengajuan($data);
			header("location:".base_url()."home");
		}
		
	}
	function upload($simpan,$nama){
		$sub = substr($simpan,-5);
		$string = uniqid().$sub;

		$target_dir = "assets/foto/";
		$target_file = $target_dir . $string;
		$uploadOk = 1;
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

		if(isset($_POST["submit"])) {
			$check = getimagesize($_FILES[$nama]["tmp_name"]);
			if($check !== false) {
				$uploadOk = 1;
			} else {
				$uploadOk = 0;
			}
		}

		if($imageFileType != "pdf" && $imageFileType != "docx" && $imageFileType != "PDF" && $imageFileType != "DOCX" ) {
			echo "<script> alert ('Format Harus JPG, JPEG, PNG & GIF !')</script>";
			$uploadOk = 0;
		}
		if ($uploadOk == 0) {
			echo "<script> alert ('Error Waktu Uploads !')</script>";
			//echo "<script> window.location.assign('mhs.php');</script>";
		} else {
			if (move_uploaded_file($_FILES[$nama]["tmp_name"], $target_file)) {
				// $this->Home_model->saveupload($kolom,$string);
				$op = 1;
			} else {
				//echo "<script> alert ('Error Waktu Upload,Silahkan upload ulang !')</script>";

			}
		}
		if ($op == 1) {
			return $string;
		}else {
			return "";
		}
	}
	public function addPenelitian(){
		date_default_timezone_set("Asia/Jakarta");
		$surat = "Surat Permohonan Izin Penelitian";
		$nim = $this->session->userdata('nim');
		$nama = $this->session->userdata('nama');
		$prodi = $this->session->userdata('prodi');
		$instansi = $this->input->post('instansi');
		$alamat = $this->input->post('alamat');
		$judul = $this->input->post('judul');
		$tgl = $this->input->post('tgl');
		$now = date("Y-m-d H:i:s");
		$data = array(
				 'nim' => $nim ,
				 'kode_surat' => '6' ,
				 'judul_penelitian' => $judul ,
				 'tgl_penelitian' => $tgl ,
				 'ket_instansi' => $instansi ,
				 'alamat_instansi' => $alamat ,
				 'jenis_surat' => $surat ,
				 'nama' => $nama ,
				 'prodi' => $prodi ,
				 'tgl_submit' => $now ,
				 'status' => "1"
			);

			// print_r ($data);
			$cek = $this->Home_model->cekantri($nim,2);
			if($cek == 1){
					echo "<script>alert ('Maaf Anda Sudah Pesan !!!');window.location.href = '".base_url()."home';</script>";
			}else {
				$this->Home_model->savePengajuan($data);
				header("location:".base_url()."home");
			}
	}
	public function addMagang(){
		date_default_timezone_set("Asia/Jakarta");
		$surat = "Surat Pengantar Magang";
		$nim = $this->session->userdata('nim');
		$nama = $this->session->userdata('nama');
		$prodi = $this->session->userdata('prodi');
		$instansi = $this->input->post('instansi');
		$alamat = $this->input->post('alamat');
		$mulai_tgl = $this->input->post('mulai_tgl');
		$tgl_akhir = $this->input->post('tgl_akhir');
		$now = date("Y-m-d H:i:s");
		$data = array(
				 'nim' => $nim ,
				 'kode_surat' => '5' ,
				 'tgl_penelitian' => $mulai_tgl ,
				 'tgl_akhir' => $tgl_akhir ,
				 'ket_instansi' => $instansi ,
				 'alamat_instansi' => $alamat ,
				 'jenis_surat' => $surat ,
				 'nama' => $nama ,
				 'prodi' => $prodi ,
				 'tgl_submit' => $now ,
				 'status' => "1"
			);

			// print_r ($data);
			$cek = $this->Home_model->cekantri($nim,2);
			if($cek == 1){
					echo "<script>alert ('Maaf Anda Sudah Pesan !!!');window.location.href = '".base_url()."home';</script>";
			}else {
				$this->Home_model->savePengajuan($data);
				header("location:".base_url()."home");
			}
	}
	public function addSaktif(){
		date_default_timezone_set("Asia/Jakarta");
		$surat = "Surat Keterangan Aktif dan Bebas Beasiswa";
		$nim = $this->session->userdata('nim');
		$nama = $this->session->userdata('nama');
		$prodi = $this->session->userdata('prodi');
		$now = date("Y-m-d H:i:s");
		$data = array(
				 'nim' => $nim ,
				 'kode_surat' => '1' ,
				 'jenis_surat' => $surat ,
				 'nama' => $nama ,
				 'prodi' => $prodi ,
				 'tgl_submit' => $now ,
				 'status' => "1"
			);
			// $data = $this->getProfil($nim);
			print_r ($data);
			$cek = $this->Home_model->cekantri($nim,1);
			if($cek == 1){
					echo "<script>alert ('Maaf Anda Sudah Pesan !!!');window.location.href = '".base_url()."home';</script>";
			}else {
				$this->Home_model->savePengajuan($data);
				header("location:".base_url()."home");
			}
	}
	public function addSCuti(){
		date_default_timezone_set("Asia/Jakarta");
		$surat = "Surat Permohonan Cuti Studi";
		$nim = $this->session->userdata('nim');
		$nama = $this->session->userdata('nama');
		$prodi = $this->session->userdata('prodi');
		$now = date("Y-m-d H:i:s");
		$data = array(
				 'nim' => $nim ,
				 'kode_surat' => '3' ,
				 'jenis_surat' => $surat ,
				 'nama' => $nama ,
				 'prodi' => $prodi ,
				 'tgl_submit' => $now ,
				 'status' => "1"
			);
			// print_r ($data);
			$cek = $this->Home_model->cekantri($nim,3);
			if($cek == 1){
					echo "<script>alert ('Maaf Anda Sudah Pesan !!!');window.location.href = '".base_url()."home';</script>";
			}else {
				$this->Home_model->savePengajuan($data);
				header("location:".base_url()."home");
			}
	}
	public function addSAktiftok(){
		date_default_timezone_set("Asia/Jakarta");
		$surat = "Surat Keterangan Aktif";
		$nim = $this->session->userdata('nim');
		$nama = $this->session->userdata('nama');
		$prodi = $this->session->userdata('prodi');
		$now = date("Y-m-d H:i:s");
		$data = array(
				 'nim' => $nim ,
				 'kode_surat' => '4' ,
				 'jenis_surat' => $surat ,
				 'nama' => $nama ,
				 'prodi' => $prodi ,
				 'tgl_submit' => $now ,
				 'status' => "1"
			);
			// print_r ($data);
			$cek = $this->Home_model->cekantri($nim,4);
			if($cek == 1){
					echo "<script>alert ('Maaf Anda Sudah Pesan !!!');window.location.href = '".base_url()."home';</script>";
			}else {
				$this->Home_model->savePengajuan($data);
				header("location:".base_url()."home");
			}
	}
	public function getProfil($nim){
		//  echo 'get profil mas '.$nim;
		$conn = pg_connect("host=180.250.165.150 port=5432 dbname=iainmigrasi user=iain password='ampelakademik!3'");
		//$result = pg_prepare($conn, "my_query", 'SELECT * FROM gate.sc_user WHERE username = $1 and password = $2
		$result = pg_prepare($conn, "my_query", "select a.nim,a.nama,a.semestermhs,a.kodeunit , a.alamat,a.tmplahir , a.tgllahir , a.ipk , b.namaunit ,b.kodeunitparent
																							from akademik.ms_mahasiswa a, gate.ms_unit b
																							where a.kodeunit = b.kodeunit
																							and nim = $1");
		$result = pg_execute($conn, "my_query", array($nim));
		while ($row = pg_fetch_assoc($result)){
			$semestermhs = $row['semestermhs'];
			$alamat = $row['alamat'];
			$tgllahir = $row['tgllahir'];
			$tmplahir = $row['tmplahir'];
			$ipk = $row['ipk'];
			$data = array(
					 'nim' => $row['nim'] ,
					 'nama' => $row['nama'] ,
					 'kodeunit' => $row['kodeunit'] ,
					 'prodi' => $row['namaunit'] ,
					 'kodefakultas' => $row['kodeunitparent'] ,
					 'semester' => $semestermhs ,
					 'alamat' => $alamat ,
					 'tgllahir' => $tgllahir ,
					 'tmplahir' => $tmplahir ,
					 'ipk' => $ipk
				);
		}
		return $data;
	}
	public function getfakultas($prodi = "Z"){
		// echo 'get profil mas ';
		$conn = pg_connect("host=180.250.165.150 port=5432 dbname=iainmigrasi user=iain password='ampelakademik!3'");
		//$result = pg_prepare($conn, "my_query", 'SELECT * FROM gate.sc_user WHERE username = $1 and password = $2
		$result = pg_prepare($conn, "my_query2", "select * from gate.ms_unit where kodeunit = $1");
		$result = pg_execute($conn, "my_query2", array($prodi));
		while ($row = pg_fetch_assoc($result)){
			$data = array(
					 'kodefakultas' => $row['kodeunit'] ,
					 'fakultas' => $row['namaunit'] ,
					 'dekan' => $row['ketua'] ,
					 'wadek1' => $row['pembantu1'] ,
					 'wadek2' => $row['pembantu2'] ,
					 'wadek3' => $row['pembantu3']
				);
		}
		return $data;
	}
	public function getdekan($nip,$kode){
		// echo 'get profil mas ';
		$conn = pg_connect("host=180.250.165.150 port=5432 dbname=iainmigrasi user=iain password='ampelakademik!3'");
		//$result = pg_prepare($conn, "my_query", 'SELECT * FROM gate.sc_user WHERE username = $1 and password = $2
		$result = pg_prepare($conn, "my_query3", "SELECT akademik.f_namalengkap(a.gelardepan,a.nama,a.gelarbelakang) AS hasil , b.namaunit , nip
																							FROM kepegawaian.ms_pegawai a , gate.ms_unit b
																							WHERE nip = $1
																							and b.kodeunit = $2;");
		$result = pg_execute($conn, "my_query3", array($nip,$kode));
		while ($row = pg_fetch_assoc($result)){
			$data = array(
					 'nama' => $row['hasil'] ,
					 'fakultas' => $row['namaunit'] ,
					 'nip' => $row['nip']
				);
		}
		return $data;
	}


	public function getBulan($bulan){
		if($bulan == 1){
			$bulan = "Januari";
		}else if($bulan == 2){
			$bulan = "Februari";
		}else if($bulan == 3){
			$bulan = "Maret";
		}else if($bulan == 4){
			$bulan = "April";
		}else if($bulan == 5){
			$bulan = "Mei";
		}else if($bulan == 6){
			$bulan = "Juni";
		}else if($bulan == 7){
			$bulan = "Juli";
		}else if($bulan == 8){
			$bulan = "Agustus";
		}else if($bulan == 9){
			$bulan = "September";
		}else if($bulan == 10){
			$bulan = "Oktober";
		}else if($bulan == 11){
			$bulan = "November";
		}else {
			$bulan = "Desember";
		}
		return $bulan;

	}
	public function surat(){
		$this->load->view('print/jajal');
	}
}
