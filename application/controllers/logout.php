<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends CI_Controller {
	function __construct(){
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->database();
		$this->load->model('Home_model');
		$this->load->helper(array('form','url','file','download'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

	}
	public function index(){
		$this->session->unset_userdata(array('nim' => ''));
		$this->session->unset_userdata(array('prodi' => ''));
		$this->session->unset_userdata(array('sex' => ''));
		$this->session->unset_userdata(array('nama' => ''));
		$this->session->unset_userdata(array('kodefak' => ''));
		//$this->session->unset_userdata(array('fakultas' => ''));
		header("location:admin");

	}

}
