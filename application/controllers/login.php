<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct(){
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->database();
		$this->load->model('Home_model');
		$this->load->helper(array('form','url','file','download'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

	}
	public function index(){
		$this->load->view('header');
		$this->load->view('index');
		$this->load->view('footer');
	}

	public function cek(){
		$id = $this->input->post('id_login');
		if($id == 1 ){
				$user = $this->input->post('nim');
				$password = md5($this->input->post('password'));
				$data = $this->Home_model->login($user,$password);
				// $this->session->set_userdata('nim',$user);
				// $this->session->set_userdata('sex',"L");
				// $this->session->set_userdata('prodi',"Sistem Informasi");
				// $this->session->set_userdata('nama',"M.Abdul Aziz");
				// $this->session->set_userdata('kodefak',"H");
				if($data == 0){
					$data_siakad = $this->ceksiakad($user,$password);
					// $data_siakad = 1;
					if($data_siakad == 0){
						echo "<script>alert ('Maaf Username/Password Salah !');window.location.href = '".base_url()."login';</script>";
					}else {
							echo "<script>window.location.href = '".base_url()."home';</script>";
					}
					//echo "<script>alert ('Maaf Username/Password Salah !');window.location.href = '".base_url()."login';</script>";
				}else if($data == 1){
					$this->session->set_userdata('username',$user);
					$this->session->set_userdata('id',"admin");
					echo "<script>window.location.href = '".base_url()."admin';</script>";
				}else {
					echo "<script>window.location.href = '".base_url()."login';</script>";
				}
		}else {
			header("location:".base_url()."login");
		}
	}

	public function ceksiakad($username,$password){
		$username = strtoupper($username);
		$conn = pg_connect("host=180.250.165.150 port=5432 dbname=iainmigrasi user=iain password='ampelakademik!3'");
		//$result = pg_prepare($conn, "my_query", 'SELECT * FROM gate.sc_user WHERE username = $1 and password = $2');

		$result = pg_prepare($conn, "my_query", "select a.nim, a.sex , c.nama_program_studi,a.nama , d.kodeunitparent
												from akademik.ms_mahasiswa a , gate.sc_user b , akademik.ak_prodi c , gate.ms_unit d
												where a.nim = b.username
												and a.kodeunit = c.kodeunit
												and d.kodeunit = a.kodeunit
												and b.username = $1
												and b.password = $2;");
		$result = pg_execute($conn, "my_query", array($username,$password));
		while ($row = pg_fetch_assoc($result)){
			$user = $row['nim'];
			$sex = $row['sex'];
			$prodi = $row['nama_program_studi'];
			$nama = $row['nama'];
			$kodefak = $row['kodeunitparent'];
		}

		if($user == null || $user == ""){
			$datas = 0;
		}else {
			if ($kodefak == "G" || $kodefak =="I") {
				$datas = 1;
				$this->session->set_userdata('nim',$user);
				$this->session->set_userdata('sex',$sex);
				$this->session->set_userdata('prodi',$prodi);
				$this->session->set_userdata('nama',$nama);
				$this->session->set_userdata('kodefak',$kodefak);
			}else {
				echo "<script>alert ('Maaf anda bukan mahasiswa FEBI atau Fisip !');window.location.href = '".base_url()."login';</script>";
			}
		}
		return $datas;
	}



}
