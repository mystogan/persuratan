<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct(){
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->database();
		$this->load->model('Home_model');
		$this->load->helper(array('form','url','file','download'));
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		$user = $this->session->userdata('username');
		if($user == null){
			header("location:".base_url()."login");
		}

	}
	public function index(){
		$user = $this->session->userdata('username');
		$data['antri'] = $this->Home_model->getPrint(1);
		$data['menunggu'] = $this->Home_model->getPrint(2);
		$data['all'] = $this->Home_model->getPrint();
		$this->load->view('header');
		$this->load->view('formstaff',$data);
		$this->load->view('footer');
	}
	public function logout(){
		$this->session->unset_userdata(array('username' => ''));
		$this->session->unset_userdata(array('id' => ''));
		header("location:../admin");

	}

}
